package edu.straus;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringCalcTest {
    StringCalc calc = new StringCalc();

    @Test
    void addEmptyTest() {
        String testString = "";
        int sum = calc.Add(testString);

        assertEquals(0, sum);
    }

    @Test
    void addSingleTestOne() {
        String testString = "1";
        int sum = calc.Add(testString);

        assertEquals(1, sum);
    }

    @Test
    void addSingleTestTwo() {
        String testString = "10";
        int sum = calc.Add(testString);

        assertEquals(10, sum);
    }

    @Test
    void addSingleTestThree() {
        String testString = "100";
        int sum = calc.Add(testString);

        assertEquals(100, sum);
    }

    @Test
    void addTwoTestOne() {
        String testString = "1,2";
        int sum = calc.Add(testString);

        assertEquals(3, sum);
    }

    @Test
    void addTwoTestTwo() {
        String testString = "10,2";
        int sum = calc.Add(testString);

        assertEquals(12, sum);
    }

    @Test
    void addTwoTestThree() {
        String testString = "1,20";
        int sum = calc.Add(testString);

        assertEquals(21, sum);
    }

    @Test
    void addTwoTestFour() {
        String testString = "10,20";
        int sum = calc.Add(testString);

        assertEquals(30, sum);
    }

    @Test
    void addAnyTestOne() {
        String testString = "1,2,3,5";
        int sum = calc.Add(testString);

        assertEquals(11, sum);
    }

    @Test
    void addAnyTestTwo() {
        String testString = "1,2,3,5,7,3,5,3,2";
        int sum = calc.Add(testString);

        assertEquals(31, sum);
    }

    @Test
    void addAnyTestThree() {
        String testString = "15,23,36,51";
        int sum = calc.Add(testString);

        assertEquals(125, sum);
    }
}