package edu.straus;

public class StringCalc {
    public int Add(String numbers){
        if(numbers.length() == 0) {
            return 0;
        } else if(numbers.split(",").length == 1) {
            return Integer.parseInt(numbers);
        } else {
            String[] nums = numbers.split(",");
            int sum = 0;
            for(int i = 0; i < nums.length; i++) {
                sum += Integer.parseInt(nums[i]);
            }
            return sum;
        }
    }
}
